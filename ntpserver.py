from multiprocessing.dummy import Pool
from socket import socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_REUSEADDR
from select import select
from time import time

from ntpclient import Packet, utc_to_ntp_bytes

import sys
import argparse

NTP_UTC_OFFSET = 2208988800
MAX_SNTP_PACKET_SIZE = 68 * 32


def get_args_parser():
    parser = argparse.ArgumentParser(description="Lying SNTP server.")
    parser.add_argument("-d", "--delay", help="Lying delay in seconds (default 0)",
                        default=0, type=int)
    parser.add_argument("-p", "--port",
                        help="Listening port (default 123)",
                        default=123, type=int)
    return parser


def get_lying_packet(packet: Packet, lying_offset: int) -> Packet:
    packet.leap = 3
    packet.mode = 4
    packet.origin = packet.transmit

    receive_time = time() + lying_offset
    transmit_time = time() + lying_offset

    return Packet(leap=3, mode=4, origin=packet.transmit_binary,
                  receive=utc_to_ntp_bytes(receive_time), 
                  transmit=utc_to_ntp_bytes(transmit_time))


def send_lying_packet(sock, input_packet, delay, addr):
    lying_packet = get_lying_packet(input_packet, delay)
    sock.sendto(lying_packet.to_binary(), addr)


def main():
    parser = get_args_parser()
    args = parser.parse_args()
    try:
        with socket(AF_INET, SOCK_DGRAM) as sock:
            sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
            sock.bind(('', args.port))

            with Pool(processes=4) as pool:
                while True:
                    read_socks, _, _ = select([sock], [], [], 3)

                    if read_socks:
                        data, addr = sock.recvfrom(MAX_SNTP_PACKET_SIZE)
                        print(addr[0])
                        input_packet = Packet.from_binary(data)

                        params = (sock, input_packet, args.delay, addr)
                        pool.apply_async(send_lying_packet, params)

    except KeyboardInterrupt as e:
        print('Bye...')
        sys.exit(0)
    except PermissionError:
        print('Sorry. Permission denied.'.format(args.port))
        sys.exit(-42)
    except:
        print('Sorry. Someting wrong. Try it again later.')
        sys.exit(-42)


if __name__ == '__main__':
    main()

